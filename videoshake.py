#!/usr/bin/python3
# vim:fileencoding=utf-8:sw=4:et

from __future__ import print_function, unicode_literals, absolute_import, division
import sys
import os
import io
import re
import logging
import queue
import threading
import subprocess
import collections
import gi
gi.require_version("Gtk", "3.0")
gi.require_version('PangoCairo', '1.0')
from gi.repository import GObject, Gtk, Gio, GLib
from gi.repository import Gdk, PangoCairo, Pango
import cairo

NATIVE=sys.getfilesystemencoding()

APPNAME = "VideoShake"
APPID = "{}.{}.{}".format("org", "mozbugbox", APPNAME)
FFMPEG_BIN = "/usr/bin/ffmpeg"
FFPROBE= "/usr/bin/ffprobe"

timestamp_re = re.compile(r"^((-?\d+):)?((\d+):)?(\d+(\.\d+)?)$")
def timestamp2seconds(stamp):
    """Convert timestamp 00:00:00.00 to seconds"""
    parts = stamp.strip().split(":")
    parts.reverse()
    sec = 0
    for i, p in enumerate(parts):
        sec += float(p)*60**i
    return sec

def seconds2timestamp(secs, full=False):
    """Format seconds in human readable form 00:00:00.00"""
    r = secs % 3600
    m = int(r // 60)
    s = r % 60
    h = 0
    if secs > 3600 or full:
        h = int(secs // 3600)
        t = "{:02d}:{:02d}:{:02.2f}".format(h, m, s)
    elif secs > 60:
        t = "{:02d}:{:02.2f}".format(m, s)
    else:
        t = "{:.02f}s".format(s)
    return t

def timestamp_norm(stamp):
    """Normalize timestamp to standard representation"""
    sec = timestamp2seconds(stamp)
    t = seconds2timestamp(sec, True)
    return t

class TextImage:
    """Create icon image out of given text"""
    def __init__(self):
        self._test_cairo_gi()
        self.size = 64
        self.background = (1.0, 0.1, 0.1, 1.0) # solid red
        self.foreground = (1.0, 1.0, 0.0, 1.0)
        self.round_corner = True
        self.margin = -1
        self.fontname = "Sans Serif bold"

    def _test_cairo_gi(self):
        from gi.repository import cairo as gicairo
        log.debug("gi.cairo: {}".format(gicairo._version))

    def draw(self, text="50", surface=None):
        """Draw text as image"""
        # set icon image.
        size = self.size
        if surface is None:
            surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, size*2, size*2)
        ctx = cairo.Context(surface)

        def draw_rounded(cr, area, radius):
            """ draws rectangles with rounded (circular arc) corners """
            from math import pi
            a,b,c,d=area
            cr.arc(a + radius, c + radius, radius, 2*(pi/2), 3*(pi/2))
            cr.arc(b - radius, c + radius, radius, 3*(pi/2), 4*(pi/2))
            cr.arc(b - radius, d - radius, radius, 0*(pi/2), 1*(pi/2))  # ;o)
            cr.arc(a + radius, d - radius, radius, 1*(pi/2), 2*(pi/2))
            cr.close_path()
            cr.fill()
            cr.stroke()

        ctx.set_source_rgba(*self.foreground)

        pango_layout = PangoCairo.create_layout(ctx)
        fd = Pango.FontDescription(self.fontname)
        if isinstance(text, bytes):
            text = text.decode("UTF-8")
        num_of_char = len(text)
        fd.set_size(size*Pango.SCALE/num_of_char)
        pango_layout.set_font_description(fd)
        pango_layout.set_text(text, -1)

        # Centralize the text
        ink_extents, logic_extents = pango_layout.get_pixel_extents()
        ext = ink_extents

        text_size = size*2/3.0
        if self.margin >= 0:
            text_size = float(size)

        hscale = text_size/ext.width
        vscale = text_size/ext.height
        # more than 1 word, stretch vertically else keep aspect
        if (float(ext.height)/ext.width) > 0.67:
            vscale = min(hscale, vscale)
            hscale = vscale
        margin_x = (size - hscale*ext.width)/2.0
        margin_y = (size - vscale*ext.height)/2.0

        ctx.save()

        matrix = cairo.Matrix()
        matrix.translate(margin_x, margin_y)
        ctx.transform(matrix)
        matrix = cairo.Matrix()
        matrix.scale(hscale, vscale)

        ctx.transform(matrix)

        ctx.move_to(-ext.x, -ext.y)

        PangoCairo.update_layout(ctx, pango_layout)
        PangoCairo.show_layout(ctx, pango_layout)

        ctx.fill()
        ctx.restore()

        # Draw background
        ctx.set_operator(cairo.OPERATOR_DEST_OVER)
        ctx.set_source_rgba(*self.background)
        x = y = 0
        w = h = size
        if self.round_corner:
            draw_rounded(ctx, (x, x+w, y, y+h), min(margin_x, margin_y))
        else:
            ctx.rectangle(x, y, w, h)
            ctx.fill()

        pixbuf = Gdk.pixbuf_get_from_surface(surface, x, y, w, h)
        return pixbuf

class Popen(subprocess.Popen):
    """Popen with sane unblocking PIPE io using threading"""
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.bufsize = 32
        if self.stdin:
            self._input_queue = queue.Queue()
            tin = threading.Thread(target=self._write_pipe,
                    args=(self.stdin, self._input_queue))
            tin.daemon = True
            tin.start()
        if self.stdout:
            self._output_queue = queue.Queue()
            tout = threading.Thread(target=self._read_pipe,
                    args=(self.stdout, self._output_queue))
            tout.daemon = True
            tout.start()
        if self.stderr:
            self._error_queue = queue.Queue()
            terr = threading.Thread(target=self._read_pipe,
                    args=(self.stderr, self._error_queue))
            terr.daemon = True
            terr.start()

    def _write_pipe(self, pipe, aqueue):
        """Thread that write to a subprocess.PIPE"""
        while not pipe.closed and pipe.writable():
            try:
                data = aqueue.get(True, timeout=1)
                pipe.write(data)
                pipe.flush()
                aqueue.task_done()
            except queue.Empty:
                pass

    def _read_pipe(self, pipe, queue):
        """Thread that write to a subprocess.PIPE"""
        while pipe.readable():
            data = pipe.read(self.bufsize)
            if not data:
                break
            queue.put(data)
        pipe.close()

    def _read_queue(self, aqueue):
        """Read data from a queue"""
        data = b""
        while True:
            try:
                data += aqueue.get(False)
                aqueue.task_done()
            except queue.Empty:
                break
        return data

    def close_input(self):
        self._input_queue.join()
        self.stdin.close()

    def _write_queue(self, data, aqueue):
        """put data into a queue"""
        aqueue.put(data)

    def write_input(self, data):
        """Write data to stdin pipe"""
        self._write_queue(data, self._input_queue)

    def read_output(self):
        """read data from stdout pipe"""
        return self._read_queue(self._output_queue)

    def read_error(self):
        """read data from stderr pipe"""
        return self._read_queue(self._error_queue)

    def wait_or_kill(self, timeout=10):
        """Wait for the process to end or kill it at timeout"""
        loop_timeout = 1
        def wait_func():
            wait_count = timeout // loop_timeout
            yield True
            for i in range(wait_count):
                if self.poll() is not None:
                    break
                yield True
            else:
                self.kill()

            yield False

        iter_wait = wait_func()
        GLib.timeout_add_seconds(loop_timeout, next, iter_wait)

    def quit(self):
        """Send SIGQUIT"""
        import signal
        self.send_signal(signal.SIGQUIT)

    def interrupt(self):
        """Send SIGINT"""
        import signal
        self.send_signal(signal.SIGINT)

def video_info(fname):
    """Get video information with ffprobe"""
    stream_keys = "width,height"
    cmd = [
            'ffprobe', '-v', 'error',
            '-show_entries', 'format=duration:stream={}'.format(stream_keys),
            '-select_streams', 'v:0',
            '-of', 'default=noprint_wrappers=1:nokey=0',
            fname,
            ]
    run = subprocess.run(cmd, stdout=subprocess.PIPE,
            universal_newlines=True)
    entries = run.stdout.splitlines()
    vinfo = {k:v for k,s,v in (e.partition("=") for e in entries) if s}
    return vinfo

def parse_ffmpeg_stderr(progress, data, header):
    """Parse ffmpeg stderr message"""
    frame_tag = b"frame="
    frame_len = len(frame_tag)

    old_data = b""
    progress_list = []
    if not progress:
        n = 0
        while True:
            # data always starts from a new line
            if data[n:n+frame_len] == frame_tag:
                data = data[n:]
                progress = True
                break
            idx = data.find(b"\n", n)
            if idx >= 0:
                idx += 1
                line = data[n:idx]
                header.append(line)
                log.debug(line.decode(NATIVE).rstrip("\n"))
                n = idx
            else:
                old_data = data[n:]
                break

    if progress and data:
        n = 0
        while True:
            idx = data.find(b"\r", n)
            if idx < 0:
                idx = data.find(b"\n", n)
            if idx < 0:
                break
            idx += 1
            if data[n:n+frame_len] != frame_tag:
                break
            line = data[n:idx]
            progress_list.append(line)
            n = idx

        old_data = data[n:]
        lead = old_data[:frame_len]
        if (len(lead) >= frame_len and lead != frame_tag):
            progress = False

            if b"\n" in old_data:
                res = parse_ffmpeg_stderr(progress, old_data,
                        header)
                progress, progress_list_r, old_data = res
                progress_list += progress_list_r
    return progress, progress_list, old_data

FFMPEGProgress = collections.namedtuple("FFMPEGProgress",
        "frame, fps, q, size, time, bitrate, speed")
class Converter(GObject.GObject):
    """FFMPEG video converter"""
    started = GObject.Signal("start")
    stopped = GObject.Signal("stop")
    progress_info = GObject.Property(type=object)

    progress_re = re.compile(r"(\w+)\s*=\s*(\S+)")
    def __init__(self):
        super().__init__()
        self.filename = None
        self.output_dir = None
        self.nice_level = 0
        self.height = -1
        self.preset = "medium"
        self.num_thread = 0
        self.tune = "film"
        self.crf = 23
        self.do_yuv420p = True
        self.h264_profile = "high"
        self.h264_level = "3.1"
        self.slice_from = None
        self.slice_duration = None

        self.origin_duration = None
        self.origin_width = None
        self.origin_height = None

        self.popen = None
        self.header = []

    @property
    def output_path(self):
        """Construct a full path for output file"""
        dirname, fname = os.path.split(self.filename)
        if self.output_dir is not None:
            dirname = self.output_dir
        base_name, ext = os.path.splitext(fname)
        fname_out = "{}-{}p{}".format(base_name, self.height, ext)
        output_path = os.path.join(dirname, fname_out)
        return output_path

    @property
    def cmd(self):
        """Generate ffmpeg convert command"""
        int_names = ["nice_level", "height", "crf", "num_thread"]
        [setattr(self, x, int(getattr(self, x))) for x in int_names]

        if not self.filename:
            return
        output_path = self.output_path

        cmd = [
                "/usr/bin/nice", "-n", self.nice_level,
                FFMPEG_BIN,
                ]

        input_opt = []
        if self.slice_from:
            slice_from = timestamp_norm(self.slice_from)
            input_opt += [ "-ss", slice_from ]
        if self.slice_duration:
            slice_duration = timestamp_norm(self.slice_duration)
            input_opt += [ "-t", slice_duration ]
        input_opt += [ "-i", self.filename ]

        video_filter = ["-vf",
                "scale=-2:{height:}".format(height=self.height),
                ]
        x264_opt = [
                "-c:v", "libx264",
                "-profile:v", self.h264_profile, "-level", self.h264_level,
                "-preset", self.preset, "-tune", self.tune,
                "-crf", self.crf,
                ]
        if self.do_yuv420p:
            x264_opt.extend(["-pix_fmt", "yuv420p"])
        misc_opt = ["-y", "-movflags", "+faststart"]
        video_opt = video_filter + x264_opt + misc_opt

        audio_opt = ["-c:a", "copy"]
        output_opt = [
                "-threads", self.num_thread,
                ]
        cmd = cmd + input_opt + audio_opt + video_opt + output_opt
        cmd.append(output_path)
        cmd = [str(x) for x in cmd]
        return cmd

    def get_video_info(self, video_file):
        """Find video information"""
        def set_vinfo(vinfo, infile):
            if self.filename == infile:
                self.origin_duration = float(vinfo["duration"])
                self.origin_width = int(vinfo["width"])
                self.origin_height = int(vinfo["height"])
            log.debug("vinfo: {}".format(vinfo))
        vinfo = video_info(video_file)
        GLib.idle_add(set_vinfo, vinfo, video_file)

    def start(self):
        """Start conversion with current settings"""
        if self.popen is not None:
            self.stop()
        self.header = []

        t = threading.Thread(target=self.get_video_info,
                args=(self.filename,))
        t.daemon = True
        t.start()

        cmd = self.cmd
        if not cmd:
            return

        log.debug(" ".join(cmd)); #return
        pipe = subprocess.PIPE
        p = Popen(cmd, stdin=pipe, stdout=pipe, stderr=pipe)

        self.popen = p
        iter_progress = self.read_progress(p)
        GLib.timeout_add_seconds(1, next, iter_progress)
        self.started.emit()
        return cmd

    def stop(self):
        """Stop the conversion"""
        popen = self.popen
        popen.write_input(b"q")
        self.popen = None
        self.stopped.emit()
        popen.wait_or_kill(30)


    def parse_progress(self, line):
        """Parse ffmpeg progress data"""
        # frame= 241 fps= 22 q=30.0 size= 1053kB time=00:00:08.28
        # bitrate=1040.5kbits/s speed=1.766x
        line = line.strip().decode(NATIVE)
        info = self.progress_re.findall(line)
        info = {k:v for k, v in info}
        if "Lsize" in info:
            info["size"] = info["Lsize"]
        for k in ["frame"]:
            info[k] = int(info[k])
        for k in ["q", "fps"]:
            info[k] = float(info[k])
        info["time"] = timestamp2seconds(info["time"])

        log.debug(line)

        info_dict = {x:info[x] for x in FFMPEGProgress._fields}
        progress_info = FFMPEGProgress(**info_dict)
        self.progress_info = progress_info
        log.debug(progress_info)

    def read_progress(self, popen):
        """Generator method to read ffmpeg conversion output"""
        progress = False # progress flag
        old_data = b""
        header = self.header
        yield True
        while True:
            outs = popen.read_output()
            errs = popen.read_error()
            if not outs and not errs:
                if popen.poll() is not None:
                    if popen == self.popen:
                        self.popen = None
                        self.stopped.emit()
                    break
                else:
                    yield True
                    continue
            data = old_data + errs
            res = parse_ffmpeg_stderr(progress, data, header)
            progress, progress_list, old_data = res
            if len(progress_list) > 0:
                self.parse_progress(progress_list[-1])

            yield True
        yield False

def marshall_variant(variant_type, val):
    """Marshall a python val to GLib.Variant"""
    if isinstance(val, GLib.Variant):
        return val
    if variant_type and variant_type.is_basic():
        val = GLib.Variant(variant_type.dup_string(), val)
    return val

# https://gist.github.com/mgedmin/0cd9e0ab5cb51d833289
def make_option(long_name, short_name=None, flags=0, arg=GLib.OptionArg.NONE,
                arg_data=None, description=None, arg_description=None):
    # surely something like this should exist inside PyGObject itself?!
    option = GLib.OptionEntry()
    option.long_name = long_name.lstrip('-')
    option.short_name = 0 if not short_name else ord(short_name.lstrip('-'))
    option.flags = flags
    option.arg = arg
    option.arg_data = arg_data
    option.description = description
    option.arg_description = arg_description
    return option

def add_action_entries(gaction_map, entries, *user_data):
    """Add action entries to GActionMap,
    GActionMap's are Gtk.Application, Gtk.ApplicationWindow"""
    def _process_action(name, activate=None, parameter_type=None,
            state=None, change_state=None):
        if state is None:
            action = Gio.SimpleAction.new(name, parameter_type)
        else:
            state = marshall_variant(parameter_type, state)
            action = Gio.SimpleAction.new_stateful(name,
                    parameter_type, state)
        if activate is not None:
            action.connect('activate', activate, *user_data)
        if change_state is not None:
            action.connect('change-state', change_state, *user_data)
        gaction_map.add_action(action)
    for e in entries:
        _process_action(*e)

class ActionControlBase:
    """Base Class to handle GAction callbacks"""
    def __init__(self, action_map):
        self.action_map = action_map

    def get_action(self, action_name):
        """get action object by  name"""
        action = self.action_map.lookup_action(action_name)
        return action

    def do_action(self, action_name, *args):
        action = self.get_action(action_name)
        if action is None:
            return

        args_new = []
        vtype = action.get_parameter_type()
        if len(args) > 0:
            for arg in args:
                args_new.append(marshall_variant(vtype, arg))

        action.activate(*args_new)

    def change_action_state(self, action_name, value):
        action = self.get_action(action_name)
        vtype = action.get_state_type()
        value = marshall_variant(vtype, value)
        if action is not None:
            action.change_state(value)

class WindowControl(ActionControlBase):
    """Contain actions for GtkApplicationWindow"""
    def __init__(self, gapp):
        super().__init__(gapp)

    @property
    def application(self):
        return self.action_map.application

    def load_actions(self):
        """Setup actions for the action_map"""
        simple_action_names = [
                "convert_video",
                "stop_convert",
                "open",
                "open_dir",
            ]
        action_entries = [[x, getattr(self, x)]
                for x in simple_action_names]
        add_action_entries(self.action_map, action_entries)

    def convert_video(self, action, param):
        app = self.application
        conv = app.converter
        conv.filename = app.input_button.get_filename()
        if not conv.filename:
            return
        conv.output_dir = app.output_button.get_filename()
        conv.nice_level = app.nice_button.get_value()
        conv.height = app.height_button.get_value()
        conv.preset = app.speed_combo.get_active_text()
        conv.num_thread = app.thread_button.get_value()
        conv.crf = app.crf_button.get_value()
        conv.slice_from = None
        conv.slice_duration = None
        if app.slice_button.props.active:
            from_text = app.from_entry.props.text
            to_text = app.to_entry.props.text
            if timestamp_re.match(from_text):
                conv.slice_from = from_text

            if timestamp_re.match(to_text):
                to_sec = timestamp2seconds(to_text)
                from_sec = (0 if not conv.slice_from else
                    timestamp2seconds(from_text))
                if to_sec > from_sec:
                    slice_duration = to_sec - from_sec
                    slice_duration = seconds2timestamp(slice_duration, True)
                    conv.slice_duration = slice_duration

        conv.start()

    def stop_convert(self, action, param):
        app = self.application
        conv = app.converter
        conv.stop()

    def open(self, action, param):
        """Open input file dialog"""
        kids = self.application.input_button.get_children()
        for k in kids:
            if isinstance(k, Gtk.Button):
                k.clicked()
                break

    def open_dir(self, action, param):
        """Open output directory filechooser dialog"""
        kids = self.application.output_button.get_children()
        for k in kids:
            if isinstance(k, Gtk.Button):
                k.clicked()
                break

class ApplicationControl(ActionControlBase):
    """Contain actions for GtkApplication"""

    @property
    def application(self):
        """Return the Application object"""
        return self.action_map.application

    def load_actions(self):
        """Setup actions for the action_map"""
        simple_action_names = [
                "quit",
            ]
        action_entries = [[x, getattr(self, x)]
                for x in simple_action_names]
        add_action_entries(self.action_map, action_entries)

    def load_accels(self):
        """Load shortcut/hotkeys"""
        accel_maps = [
                ["app.quit", ["<Control>q"]],

                ["win.open", ["<Control>i"]],
                ["win.open_dir", ["<Control>d"]],

                ["win.convert_video", ["<Control>c"]],
                ["win.stop_convert", ["<Control>s"]],
            ]

        gapp = self.application.gapp
        for act, accels in accel_maps:
            gapp.set_accels_for_action(act, accels)

    def quit(self, action, param):
        self.application.quit()

# FileChooser filter for video files
FILTER_PATTERNS = [
        ("Video", [
            "*.mp4",
            "*.ts",
            "*.mkv",
            "*.avi",
            "*.webm",
            "*.flv",
            "*.f4v",
            ]),
        ("*.mp4", ["*.mp4"]),
        ("*.avi", ["*.avi"]),
        ("*.flv", ["*.flv"]),
        ("*.f4v", ["*.f4v"]),
        ("*.ts", ["*.ts"]),
        ("*.mkv", ["*.mkv"]),
        ("*.webm", ["*.webm"]),
        ("All", ["*.*"]),
    ]
def create_video_filters():
    # FileChooser filters
    filters = []
    for name, patterns in FILTER_PATTERNS:
        ff = Gtk.FileFilter()
        ff.set_name(name)
        patterns_u = [x.upper() for x in patterns if x.islower()]
        if len(patterns_u) > 0:
            patterns.extend(patterns_u)
        for p in patterns:
            ff.add_pattern(p)
        filters.append(ff)
    return filters

def setup_ui(app):
    """Setup main gui"""
    main_win = Gtk.ApplicationWindow()
    textimg = TextImage()
    textimg.foreground = (.94, 1, .94, 1)
    textimg.background = (.6, 0, .8, 1)
    win_icon = textimg.draw("Vs")
    main_win.set_icon(win_icon)
    app.gapp.add_window(main_win)

    grid = Gtk.Grid()
    grid.props.expand = True
    grid.props.row_spacing = 6
    grid.props.column_spacing = 6
    grid.props.margin = 6
    main_win.add(grid)

    row = 0
    input_button = Gtk.FileChooserButton("", Gtk.FileChooserAction.OPEN)
    input_button.props.hexpand = True
    input_button.props.title = "Select video file to convert"
    label = Gtk.Label("Input: ")
    label.props.halign = Gtk.Align.START
    label.props.hexpand = False
    grid.attach(label, 0, row, 1, 1)
    grid.attach(input_button, 1, row, 6, 1)

    row += 1
    output_button = Gtk.FileChooserButton("",
            Gtk.FileChooserAction.CREATE_FOLDER)
    output_button.props.hexpand = True
    output_button.props.title = "Select Directory to Save video"
    output_button.props.tooltip_text = ("Output directory.\n"
            "If None, use input directory.\n"
            "Output filename: filename-720p.mp4")
    label = Gtk.Label("Output: ")
    label.props.halign = Gtk.Align.START
    grid.attach(label, 0, row, 1, 1)
    grid.attach(output_button, 1, row, 6, 1)

    row += 1
    height_button = Gtk.SpinButton()
    height_button.set_increments(2, 20)
    height_button.set_range(360, 10240)
    height_button.props.hexpand = False
    height_button.props.halign = Gtk.Align.START
    height_button.props.tooltip_text = "Height of output video"
    label = Gtk.Label("Height: ")
    label.props.halign = Gtk.Align.START
    grid.attach(label, 0, row, 1, 1)
    grid.attach(height_button, 1, row, 1, 1)

    speed_combo = Gtk.ComboBoxText()
    speed_combo.props.halign = Gtk.Align.START
    speed_combo.props.tooltip_text = "Faster speed result in larger file"
    speeds = ("ultrafast, superfast, veryfast, faster, fast, "
    "medium, slow, slower, veryslow")
    [speed_combo.append(x.strip(), x.strip()) for x in speeds.split(",")]
    label = Gtk.Label("Speed: ")
    label.props.halign = Gtk.Align.START
    grid.attach(label, 2, row, 1, 1)
    grid.attach(speed_combo, 3, row, 1, 1)

    thread_button = Gtk.SpinButton()
    thread_button.set_range(1, 64)
    thread_button.set_increments(1, 2)
    thread_button.props.halign = Gtk.Align.START
    thread_button.props.tooltip_text = "Number of thread for encoding"
    label = Gtk.Label("Threads: ")
    label.props.halign = Gtk.Align.START
    grid.attach(label, 4, row, 1, 1)
    grid.attach(thread_button, 5, row, 1, 1)

    row += 1
    orient = Gtk.Orientation
    crf_button = Gtk.Scale.new_with_range(orient.HORIZONTAL, 0, 51, 1)
    crf_button.props.tooltip_text = ("(Constant Rate Factor)\n"
            "Smaller value result in better quality, larger file size\n"
            "+6 double file size")
    label = Gtk.Label("CRF: ")
    label.props.halign = Gtk.Align.START
    grid.attach(label, 0, row, 1, 1)
    grid.attach(crf_button, 1, row, 2, 1)

    row += 1
    nice_button = Gtk.Scale.new_with_range(orient.HORIZONTAL, -20, 19, 1)
    nice_button.props.tooltip_text = "Nice level of ffmpeg process"
    label = Gtk.Label("Nice Level: ")
    label.props.halign = Gtk.Align.START
    grid.attach(label, 0, row, 1, 1)
    grid.attach(nice_button, 1, row, 2, 1)

    row += 1
    slice_button = Gtk.CheckButton("Slice Video")
    grid.attach(slice_button, 0, row, 2, 1)

    row += 1
    from_entry = Gtk.Entry()
    from_entry.props.tooltip_text = "Video start time: [00:00:]00[.000]"
    from_entry.props.sensitive = False
    slice_button.bind_property("active", from_entry, "sensitive")

    label = Gtk.Label("From: ")
    label.props.halign = Gtk.Align.START
    grid.attach(label, 0, row, 1, 1)
    grid.attach(from_entry, 1, row, 1, 1)

    to_entry = Gtk.Entry()
    to_entry.props.tooltip_text = "Video stop time: [00:00:]00[.000]"
    to_entry.props.sensitive = False
    slice_button.bind_property("active", to_entry, "sensitive")
    label = Gtk.Label("To: ")
    label.props.halign = Gtk.Align.START
    grid.attach(label, 2, row, 1, 1)
    grid.attach(to_entry, 3, row, 1, 1)

    row += 1
    extra_entry = Gtk.Entry()
    extra_entry.props.hexpand = True
    label = Gtk.Label("Extra Option: ")
    label.props.halign = Gtk.Align.START
    grid.attach(label, 0, row, 1, 1)
    grid.attach(extra_entry, 1, row, 6, 1)

    row += 1
    convert_progressbar = Gtk.ProgressBar()
    #convert_progressbar.props.hexpand = True
    convert_progressbar.props.show_text = True
    grid.attach(convert_progressbar, 0, row, 7, 1)

    row += 1
    fps_entry = Gtk.Entry()
    fps_entry.props.sensitive = False
    fps_entry.props.hexpand = False
    fps_entry.props.halign = Gtk.Align.START
    fps_entry.props.width_chars = 8
    label = Gtk.Label("FPS: ")
    label.props.halign = Gtk.Align.START
    grid.attach(label, 0, row, 1, 1)
    grid.attach(fps_entry, 1, row, 1, 1)

    rate_entry = Gtk.Entry()
    rate_entry.props.sensitive = False
    rate_entry.props.hexpand = False
    rate_entry.props.halign = Gtk.Align.START
    rate_entry.props.width_chars = 12
    label = Gtk.Label("Rate: ")
    label.props.halign = Gtk.Align.START
    grid.attach(label, 2, row, 1, 1)
    grid.attach(rate_entry, 3, row, 1, 1)

    convert_time_entry = Gtk.Entry()
    convert_time_entry.props.sensitive = False
    label = Gtk.Label("Time: ")
    label.props.halign = Gtk.Align.START
    grid.attach(label, 4, row, 1, 1)
    grid.attach(convert_time_entry, 5, row, 1, 1)

    row += 1
    start_button = Gtk.Button("_Start")
    start_button.props.use_underline = True
    start_button.props.hexpand = False
    start_button.props.halign = Gtk.Align.START
    grid.attach(start_button, 0, row, 1, 1)

    video_filters = create_video_filters()
    [input_button.add_filter(vf) for vf in video_filters]
    [output_button.add_filter(vf) for vf in video_filters]

    exports = ["main_win", "input_button", "output_button",
            "height_button", "speed_combo", "thread_button",
            "crf_button", "nice_button", "extra_entry",
            "slice_button", "from_entry", "to_entry",
            "fps_entry", "rate_entry", "convert_time_entry",
            "convert_progressbar", "start_button",
            ]

    lvar = locals()
    for name in exports:
        setattr(app, name, lvar[name])

class Application:
    def __init__(self):
        self.converter = Converter()
        self.application_control = None
        self.setup_app()

    def setup_app(self):
        self.gapp = Gtk.Application(application_id=APPID,
                flags=Gio.ApplicationFlags.HANDLES_OPEN|
                Gio.ApplicationFlags.NON_UNIQUE
                )

        cmdline_options = [
                make_option("--debug", "-D", description="Debug mode"),
                ]
        self.gapp.add_main_option_entries(cmdline_options)

        self.gapp.application = self
        self.gapp.connect("startup", self.on_app_startup)
        self.gapp.connect("handle-local-options",
                self.on_app_handle_local_options)
        self.gapp.connect("activate", self.on_app_activate)
        self.gapp.connect("open", self.on_open)
        self.gapp.connect("shutdown", self.on_shutdown)
        self.gapp.register()

    def on_app_startup(self, gapp):
        setup_ui(self)
        self.connect_signals()
        self.main_win.application = self
        self.window_control = WindowControl(self.main_win)
        self.application_control = ApplicationControl(self.gapp)
        self.window_control.load_actions()
        self.application_control.load_actions()

        #menu_path = os.path.join(const.PKG_DATA_DIR, const.MENU_FILE)
        #menu_xml = Gtk.Builder.new_from_file(menu_path)

        #self.win_menu = menu_xml.get_object("win-menu")
        #app_menu = menu_xml.get_object("app-menu")
        #self.gapp.set_app_menu(app_menu)

        def _load_rest():
            """delay loading the rest to speed up startup time"""
            self.height_button.props.value = 720
            self.speed_combo.props.active_id = "medium"
            self.crf_button.props.adjustment.props.value = 24
            self.nice_button.props.adjustment.props.value = 10
            import multiprocessing
            cpu_count = multiprocessing.cpu_count()
            self.thread_button.props.value = cpu_count - 2

            self.application_control.load_accels()

        self.main_win.show_all()
        GLib.idle_add(_load_rest)

    def on_app_handle_local_options(self, gapp, options):
        if options.lookup_value("debug"):
            log.setLevel(logging.DEBUG)
        return -1

    def on_open(self, gapp, gfile_list, file_count, hint):
        self.open_files(gfile_list)

    def open_files(self, gfile_list):
        for gfile in gfile_list:
            path = gfile.get_path()
            self.input_button.set_filename(path)
            break

            #self.application_control.do_action("open_path", path)
            #dw = DocumentWindow(self, path)
            #self.gapp.add_window(dw.window_main)
            #self.actions.load_win_actions(dw.window_main)

    def on_app_activate(self, gapp):
        self.window_control.do_action("open")

    def on_shutdown(self, gapp):
        log.debug("Shutting down")

    def _on_start_button_clicked(self, w):
        if self.start_button.props.label == "_Start":
            self.window_control.do_action("convert_video")
        else:
            self.window_control.do_action("stop_convert")

    def _on_converter_start(self, gobj):
        self.start_button.props.label = "_Stop"
    def _on_converter_stop(self, gobj):
        self.start_button.props.label = "_Start"
        self.convert_progressbar.props.fraction = 0.0
        self.convert_progressbar.props.text = ""

    def _on_converter_progress_info(self, conv, gparm):
        """notify on Converter.props.progress_info"""
        pinfo = conv.progress_info
        duration = conv.origin_duration
        if conv.slice_duration:
            duration = timestamp2seconds(conv.slice_duration)

        fraction = 0.0
        remain = "-"
        if duration:
            fraction = pinfo.time / duration
            speed_rate = float(pinfo.speed.rstrip("x"))
            remain = (duration - pinfo.time)/speed_rate
            remain = seconds2timestamp(remain)

        msg = "{:.2%} -{}".format(fraction, remain)
        self.convert_progressbar.props.fraction = fraction
        self.convert_progressbar.props.text = msg
        self.fps_entry.props.text = "{:.0f}".format(pinfo.fps)
        self.rate_entry.props.text = pinfo.speed
        self.convert_time_entry.props.text = seconds2timestamp(pinfo.time)

    def connect_signals(self):
        self.start_button.connect("clicked", self._on_start_button_clicked)
        self.converter.connect("start", self._on_converter_start)
        self.converter.connect("stop", self._on_converter_stop)
        self.converter.connect("notify::progress-info",
                self._on_converter_progress_info)

    def run(self):
        self.gapp.run(sys.argv)

    def quit(self):
        self.gapp.quit()

def setup_log(log_level):
    global log
    log = logging.getLogger(__name__)
    log.setLevel(log_level)
    ch = logging.StreamHandler()
    formatter = logging.Formatter("%(levelname)s>> %(message)s")
    ch.setFormatter(formatter)
    log.addHandler(ch)

def main():
    def set_stdio_encoding(enc=NATIVE):
        import codecs; stdio = ["stdin", "stdout", "stderr"]
        for x in stdio:
            obj = getattr(sys, x)
            if not obj.encoding: setattr(sys,  x, codecs.getwriter(enc)(obj))
    set_stdio_encoding()

    log_level = logging.INFO
    #log_level = logging.DEBUG
    setup_log(log_level)
    app = Application()
    app.run()

if __name__ == '__main__':
    import signal; signal.signal(signal.SIGINT, signal.SIG_DFL)
    main()

